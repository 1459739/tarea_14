<!-- resources/views/proveedores/create.blade.php -->

<x-modal title="{{ __('Crear nuevo proveedor') }}" wire:model="agregarProveedor" focusable>
    <form class="mt-6 space-y-6" wire:submit.prevent="store">
        @csrf
        <x-validation-errors />

        <!-- Campos del formulario -->
        <div>
            <x-input.label for="RSO_PRV">{{ __('Razón social') }} *</x-input.label>
            <x-input.text id="RSO_PRV" class="block mt-1 w-full" type="text" name="RSO_PRV" wire:model.defer="RSO_PRV" autocomplete="off" maxlength="80" required autofocus />
            <x-input.message-error :messages="$errors->get('RSO_PRV')" class="mt-2" />
        </div>

        <div>
            <x-input.label for="DIR_PRV">{{ __('Dirección') }} *</x-input.label>
            <x-input.text id="DIR_PRV" class="block mt-1 w-full" type="text" name="DIR_PRV" wire:model.defer="DIR_PRV" autocomplete="off" maxlength="100" required />
            <x-input.message-error :messages="$errors->get('DIR_PRV')" class="mt-2" />
        </div>

        <div>
            <x-input.label for="TEL_PRV">{{ __('Teléfono') }}</x-input.label>
            <x-input.text id="TEL_PRV" class="block mt-1 w-full" type="text" name="TEL_PRV" wire:model.defer="TEL_PRV" autocomplete="off" maxlength="15" />
            <x-input.message-error :messages="$errors->get('TEL_PRV')" class="mt-2" />
        </div>

        <div>
            <x-input.label for="REP_PRV">{{ __('Representante') }} *</x-input.label>
            <x-input.text id="REP_PRV" class="block mt-1 w-full" type="text" name="REP_PRV" wire:model.defer="REP_PRV" autocomplete="off" maxlength="80" required />
            <x-input.message-error :messages="$errors->get('REP_PRV')" class="mt-2" />
        </div>

        <!-- Botones -->
        <div class="flex justify-end gap-4">
            <x-primary-button type="submit">
                <i class="fa-solid fa-save me-1"></i>{{ __('Guardar') }}
            </x-primary-button>
            <x-secondary-button wire:click.prevent="resetValidationAndFields">
                <i class="fa-solid fa-ban me-1"></i>{{ __('Cancelar') }}
            </x-secondary-button>
        </div>
    </form>
</x-modal>
