<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    use HasFactory;

    protected $table = 'dbo.PROVEEDOR';
    //protected $table = 'proveedores';

    protected $primaryKey = 'cod_prv';
    public $incrementing = false;
    protected $keyType = 'char';
    protected $fillable = ['cod_prv', 'rso_prv', 'dir_prv', 'tel_prv', 'cod_dis', 'rep_prv'];

    public function distrito()
    {
        return $this->belongsTo(Distrito::class, 'cod_dis', 'cod_dis');
    }

    public function ordenesCompra()
    {
        return $this->hasMany(OrdenCompra::class, 'cod_prv', 'cod_prv');
    }

    public function abastecimientos()
    {
        return $this->hasMany(Abastecimiento::class, 'cod_prv', 'cod_prv');
    }
}
